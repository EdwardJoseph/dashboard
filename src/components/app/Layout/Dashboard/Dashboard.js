import { Route } from 'react-router-dom';
import Header from './Header/Header';
import Read from './Container/Read/Read';
import Edit from './Container/Edit/Edit';
import Logout from './Container/Logout/Logout';

const Dashboard = () => {
	return (
		<>
			<Header />
			<div className={'container my-5'}>
				<main>
					<Route exact path="/">
						<Read />
					</Route>
					<Route exact path="/edit">
						<Edit />
					</Route>
					<Route exact path="/logout">
						<Logout />
					</Route>
				</main>
			</div>
		</>
	);
}

export default Dashboard;
