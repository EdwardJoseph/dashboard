import { useContext, useState, useRef } from 'react';
import InformationContext from '../../../../../../contexts/User/InformationContext';

const Edit = () => {
	// State
	const { user, setUser } = useContext(InformationContext);
	const [username, setUsername] = useState(user.username);

	// Ref
	const usernameInput = useRef(null);
	const button = useRef(null);

	// Change username (Finish)
	const changeUsernameHandler = (e) => {
		const value = usernameInput.current.value;
		const target = button.current;

		if (value.length >= 4) {
			target.innerHTML = 'نام کاربری شما با موفقیت تغییر یافت';
			target.disabled = true;

			setUser({ ...user, username: value });
		}
	}

	// Change Username value
	const changeUsernameValue = (e) => {
		const value = e.target.value;
		const target = button.current;

		setUsername(value);
		
		target.innerHTML = 'ثبت';
		target.disabled = false;
	}

	return (
		<div className={'d-block bg-white border border-1 rounded-3 shadow-sm w-100 px-3 py-4 m-0'}>
			<div class="input-group m-0 p-0">
				<input onChange={(e) => changeUsernameValue(e)} ref={usernameInput} type={"text"} className={"form-control rounded-0 rounded-end text-secondary font-weight-500 font-size-14"} placeholder={username} value={username} />
				<button ref={button} onClick={changeUsernameHandler} className={"btn btn-primary rounded-0 rounded-start"} type={"button"}>تایید</button>
				{username.length < 4 &&
					<div class="invalid-feedback d-block">نام کاربری وارد شده نامعتبر است</div>
				}
			</div>
		</div>
	);
}

export default Edit;
