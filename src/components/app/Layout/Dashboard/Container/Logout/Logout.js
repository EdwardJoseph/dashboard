import CircularProgress from '@material-ui/core/CircularProgress';


const Logout = () => {
	setTimeout(() => {
		localStorage.clear();
		window.location.href = '/';
	}, 1000);

	return (
		<div className={'d-flex align-items-center justify-content-center w-100 h-100 p-0 m-0'}>
			<CircularProgress />
		</div>
	);
}

export default Logout;
