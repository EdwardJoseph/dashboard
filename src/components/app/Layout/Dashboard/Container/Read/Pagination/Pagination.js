import clsx from 'clsx';
import { BiChevronRight, BiChevronLeft } from 'react-icons/bi';

const Pagination = ({ currentPage, postsPerPage, totalPosts, paginate, nextPage, prevPage }) => {
	const pageNumbers = [];
	for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
		pageNumbers.push(i);
	}

	// First page
	const firstPage = (currentPage === 1);

	// Last page
	const lastPage = (currentPage === Math.ceil(totalPosts / postsPerPage));

	return (
		<div className={'bg-white text-center border borer-1 rounded-3 w-100 h-100 p-4 mt-3 mb-0 mx-0'}>
			<nav style={{ direction: 'ltr' }}>
				<ul className={"pagination justify-content-center p-0 m-0"}>
					<li className={clsx({
						'disabled': firstPage,
					}, ['page-item'])}>
						<button disabled={firstPage} onClick={prevPage} className={"page-link rounded-0 rounded-start"}>
							<BiChevronLeft size={18} />
						</button>
					</li>
					{pageNumbers.map((num, i) => (
						<li key={i} className={clsx({
							'active': (currentPage === num),
						}, ['page-item'])}>
							<button onClick={() => paginate(num)} className={"page-link rounded-0"}>
								{num}
							</button>
						</li>
					))}
					<li className={clsx({
						'disabled': lastPage,
					}, ['page-item'])}>
						<button disabled={lastPage} onClick={nextPage} className={"page-link rounded-0 rounded-end"}>
							<BiChevronRight size={18} />
						</button>
					</li>
				</ul>
			</nav>
		</div>
	);
}

export default Pagination;
