import { useRef } from "react";

const Modal = ({ post, edit, close }) => {
	const titleInput = useRef(null);
	const BodyInput = useRef(null);

	// Post edit
	const postEdit = (i ,data) => {
		edit(i, data);
		close();
	}

	return (
		<div className="card position-relative shadow-sm border text-start" style={{ width: '100%', maxWidth: '768px' }}>
			<div className="card-body">
				<input ref={titleInput} style={{minHeight: '50px', direction: 'ltr'}} className={"d-block w-100 h-100 border border-1 rounded card-subtitle mb-2 text-muted text-start px-3 py-0"} defaultValue={post.title} />
				<textarea ref={BodyInput} style={{minHeight: '300px', direction: 'ltr', resize: 'none'}} className={"d-block w-100 h-100 border border-1 rounded card-text text-start text-dark mb-4 p-3"} defaultValue={post.body}></textarea>
				<div className={"btn-group"}>
					<button className={'card-link text-danger btn font-size-14 font-weight-500 rounded'} onClick={close}>Cancel</button>
					<button className={'card-link btn btn-success font-size-14 font-weight-500 rounded'} onClick={() => postEdit(post.index, {title: titleInput.current.value, body: BodyInput.current.value})} >Save</button>
				</div>
			</div>
		</div>
	);
}

export default Modal;
