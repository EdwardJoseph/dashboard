import { useState } from 'react';
import { AiOutlineEdit } from 'react-icons/ai';
import Modal from '../../../../../../../common/Modal/Modal';
import EditModal from './Modal/Modal';

const Post = (post) => {
	const [modalState, setModalState] = useState(false);

	return (
		<tr>
			<td className={'font-weight-bold'}>{post.id}</td>
			<td className={'font-weight-500'}>{post.title}</td>
			<td className={'font-weight-500'}>{post.body}</td>
			<td className={'font-weight-500 text-center'}>
				<button onClick={() => setModalState(true)} className={'border-0 bg-transparent'} type={'button'}>
					<AiOutlineEdit size={24} color={'#2196F3'} />
				</button>
			</td>
			{modalState &&
				<Modal>
					<EditModal post={{ ...post, edit: null }} edit={post.edit} close={() => setModalState(false)} />
				</Modal>
			}
		</tr>
	);
}

export default Post;
