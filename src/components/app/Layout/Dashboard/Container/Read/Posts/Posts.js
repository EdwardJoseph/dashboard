import CircularProgress from '@material-ui/core/CircularProgress';
import Post from './Post/Post';
import classes from './Posts.scss';

const Posts = ({ loading, posts, edit }) => {
	// Get posts
	const getPosts = posts.map((post, i) => (
		<Post key={i} {...post} index={i} edit={edit} />
	));

	return (
		<div className={'table-responsive'}>
			{loading
				?
				<div className={'bg-white text-center border borer-1 rounded-3 w-100 h-100 p-4 m-0'}>
					<CircularProgress color={'secondary'} />
				</div>
				:
				<table className={`${classes.table} table bg-white border border-1 rounded shadow-sm`}>
					<thead>
						<tr>
							<th style={{ width: '100px' }} scope="col">#</th>
							<th style={{ width: '325px' }} scope="col">عنوان</th>
							<th scope="col">بدنه</th>
							<th style={{ width: '50px' }} scope="col">ویرایش</th>
						</tr>
					</thead>
					<tbody>{getPosts}</tbody>
				</table>
			}
		</div>
	);
}

export default Posts;
