import { Component } from 'react';
import axios from 'axios';
import Posts from './Posts/Posts';
import Pagination from './Pagination/Pagination';
import { FiSearch } from 'react-icons/fi';
import classes from './Read.scss';

class Read extends Component {
	state = {
		posts: [],
		search: '',
		loading: false,
		currentPage: 1,
		postsPerPage: 10,
	}

	componentDidMount() {
		// Set loading
		this.setState({
			...this.state,
			loading: true,
		});

		// axios
		axios
			.get('https://jsonplaceholder.typicode.com/posts')
			.then((response) => {
				this.setState({
					...this.state,
					posts: response.data,
				});
			})
			.catch((e) => {
				console.log(e);
			})
			.then(() => {
				this.setState({
					...this.state,
					loading: false,
				});
			});
	}

	render() {
		const { currentPage, postsPerPage, posts: oldPosts, loading, search } = this.state;
		const posts = oldPosts.filter((val) => {
			if (search === "") {
				return val;
			} else if (val.title.toLowerCase().includes(search)) {
				return val;
			}

			return null;
		});

		// First and Last posts
		const indexOfLastPost = currentPage * postsPerPage;
		const indexOfFirstPost = indexOfLastPost - postsPerPage;

		// SLice posts
		const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

		// Paginate
		const paginate = pageNum => this.setState({ ...this.state, currentPage: pageNum });

		// Next page
		const nextPage = () => this.setState({ ...this.state, currentPage: this.state.currentPage + 1 });

		// Prev page
		const prevPage = () => this.setState({ ...this.state, currentPage: this.state.currentPage - 1 });

		// Edit post
		const editPost = (postID, data) => {
			const posts = [...this.state.posts];
			if (posts[postID]) posts[postID] = {...posts[postID], ...data};

			this.setState({
				...this.state,
				posts: posts,
			});
		};

		// Filter search
		const changeInputValue = (e) => {
			this.setState({
				...this.state,
				search: e.target.value,
			});
		}

		return (
			<>
				<div className={'bg-white text-center border borer-1 rounded-3 w-100 h-100 p-4 mb-3 mb-0 mx-0'}>
					<div className={`${classes.inputGroup} input-group m-0 p-0`}>
						<input value={search} onChange={(e) => changeInputValue(e)} type={'text'} className={'font-size-14 font-weight-300 text-end text-secondary bg-light form-control shadow-none rounded-0 rounded-end border border-1 border-start-0'} placeholder={'جستجو کنید ...'} />
						<span className={'input-group-text border-1 border border-end-0 rounded-0 rounded-start bg-light'}>
							<FiSearch size={18} color={'#78909C'} />
						</span>
					</div>
				</div>
				<Posts posts={currentPosts} loading={loading} edit={editPost} />
				<Pagination nextPage={nextPage} prevPage={prevPage} paginate={paginate} currentPage={this.state.currentPage} totalPosts={this.state.posts.length} postsPerPage={this.state.postsPerPage} />
			</>
		);
	}
}

export default Read;
