import { NavLink } from 'react-router-dom';
import classes from './List.scss'

const List = ({ dropdown }) => {
	return (
		<ul className={"navbar-nav my-2 my-lg-0 navbar-nav-scroll"}>
			{dropdown.map((item, i) => (
				<li key={i} className={`${classes.item} nav-item`}>
					<NavLink exact activeClassName={classes.active} className={'nav-link font-weight-500'} to={item.href}>{item.title}</NavLink>
				</li>
			))}
		</ul>
	);
}

export default List;
