import { useContext } from 'react';
import InformationContext from '../../../../../contexts/User/InformationContext'
import List from './List/List';
import classes from './Header.scss'

const Header = () => {
	const { user } = useContext(InformationContext);

	return (
		<header className={`${classes.header} bg-white shadow-sm border p-0 m-0`} position="relative">
			<nav className="navbar navbar-expand bg-white">
				<div className="container-fluid">
					<div className={"collapse navbar-collapse justify-content-start"}>
						<List dropdown={[
							{ title: 'صفحه اصلی', href: '/' },
							{ title: 'ویرایش اطلاعات', href: '/edit' },
							{ title: 'خروج از سیستم', href: '/logout' },
						]} />
					</div>
					<a className={`${classes.brand} navbar-brand font-weight-bold font-size-18`} href={'/'}>{user.username}</a>
				</div>
			</nav>
		</header>
	);
}

export default Header;
