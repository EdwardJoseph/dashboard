import { useState, useContext, useRef, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Fab from '@material-ui/core/Fab';
import Checkbox from '@material-ui/core/Checkbox';
import CircularProgress from '@material-ui/core/CircularProgress';
import AuthContext from '../../../../../contexts/User/AuthContext';
import InformationContext from '../../../../../contexts/User/InformationContext';
import { BsEyeSlash, BsEye } from 'react-icons/bs';
import classes from './Form.scss';

const Form = () => {
	// Context
	const { login } = useContext(AuthContext);
	const { user, setUser } = useContext(InformationContext);

	// State
	const [remember, setRemember] = useState(false);
	const [hasError, setHasError] = useState(false);
	const [submit, setSubmit] = useState(false);
	const [passwordInputState, setPasswordInputState] = useState({
		type: 'password',
		value: user.password,
	});
	const [usernameInputState, setUsernameInputState] = useState(user.username);

	// Ref
	const usernameInput = useRef(null);
	const passwordInput = useRef(null);
	const submitButton = useRef(null);

	// Change event handler
	const rememberEventHandler = () => {
		setRemember(!remember);
	}

	// Custom checkbox
	const PrimaryCheckbox = withStyles({
		root: {
			color: '#d3d6d8',
			'&$checked': {
				color: '#0d6efd',
			},
		},
		checked: {},
	})((props) => <Checkbox color="default" {...props} />);

	// Validate form
	const validateForm = () => {
		if (usernameInput.current.value.length <= 4) return false;
		if (passwordInput.current.value.length <= 8) return false;

		return true;
	}

	// Submit form
	const loginHandler = (e) => {
		e.preventDefault();
		setSubmit(true);

		setTimeout(() => {
			if (validateForm()) {
				// Update user
				setUser({
					username: usernameInput.current.value,
					password: passwordInput.current.value,
				});

				// Redirect
				login(true);
			} else {
				setHasError(true);
				setSubmit(false);
			}
		}, 1000);

		// prev redirect
		return false;
	}

	// Change password input type
	const changePasswordInputType = () => {
		const newType = (passwordInputState.type === 'password') ? 'text' : 'password';
		setPasswordInputState({ ...passwordInputState, type: newType });
	}

	// Change password handler
	const changePasswordHandler = (e) => {
		setPasswordInputState({ ...passwordInputState, value: e.target.value });
	}

	// Change username handler
	const changeUsernameHandler = (e) => {
		setUsernameInputState(e.target.value);
	}

	// Submit state if change
	useEffect(() => {
		submitButton.current.disabled = submit;
	}, [submit]);


	return (
		<form onSubmit={(e) => loginHandler(e)} className={`text-end  w-100 h-100 p-0 m-0`} method={'GET'}>
			<div className={`${classes.title} w-100 mb-5 mt-0 mx-0`}>
				<h1 className={'font-weight-bold h4 w-100 m-0 p-0'}>ورود</h1>
			</div>
			<div className={`${classes.description} w-100 mb-4 mt-0 mx-0 p-0`}>
				<h4 className={'font-weight-500 font-size-18 w-100 mb-2 mt-0 mx-0 p-0'}>وارد حساب خود شوید</h4>
				<p className={'font-weight-200 font-size-14 text-justify p-0 m-0'}>از اینکه ما را انتخاب کرده‌اید تشکر می‌کنیم. اجازه دهید بهترین‌ها را برای شما فراهم کنیم.</p>
			</div>
			{hasError &&
				<div className={'alert alert-danger'}>مقادیر وارد شده نامعتبر می‌باشند</div>
			}
			<div className={`${classes.inputBox} w-100 h-100 p-0 m-0`}>
				<div className={`${classes.inputGroup} input-group m-0 p-0`}>
					<label htmlFor={'username'} className={'font-size-14 font-weight-300 d-block w-100 mb-2 mt-0 mx-0 p-0'}>نام کاربری</label>
					<input onChange={changeUsernameHandler} ref={usernameInput} id={'username'} value={usernameInputState} className={'border border-1 rounded-3 w-100 px-3 py-0 m-0'} type={'text'} placeholder={'نام کاربری خود را وارد نمایید'} />
				</div>

				<div className={`${classes.inputGroup} input-group mt-3 mb-1 mx-0 p-0`}>
					<label htmlFor={'password'} className={'font-size-14 font-weight-300 d-block w-100 mb-2 mt-0 mx-0 p-0'}>رمز عبور</label>
					<div className={'position-relative w-100 p-0 m-0'}>
						<input onChange={changePasswordHandler} value={passwordInputState.value} ref={passwordInput} id={'password'} className={'border border-1 rounded-3 w-100 ps-5 pe-3 py-0 m-0'} type={passwordInputState.type} placeholder={'رمز عبور خود را وارد نمایید'} />
						{(passwordInputState.value.length > 1) &&
							<span onClick={changePasswordInputType} className={`${classes.toggleType} position-absolute m-0 p-0`}>
								{(passwordInputState.type === 'password')
									? <BsEyeSlash size={18} color={'#90A4AE'} />
									: <BsEye size={18} color={'#90A4AE'} />}
							</span>
						}
					</div>
				</div>
				<div className={'d-flex justify-content-between w-100 mt-2 mb-0 mx-0 p-0'}>
					<div className={`${classes.checkbox} d-inline-block w-auto m-0 p-0`}>
						<FormControlLabel
							control={
								<PrimaryCheckbox checked={remember} onChange={rememberEventHandler} name="remember" />
							}
							className={'m-0 p-0'}
							label="من را به یاد بیاور"
						/>
					</div>
					<div className={`${classes.reset} d-inline-block w-auto m-0 p-0`}>
						<a className={'font-weight-500 font-size-14'} href="/">رمز عبور خود را فراموش کرده‌اید؟</a>
					</div>
				</div>
			</div>
			<div className={`${classes.submit} mt-4 mb-5 mx-0 p-0`}>
				<Fab ref={submitButton} type={'submit'} className={'font-weight-500 font-size-14 d-flex align-items-center w-100 bg-primary rounded-3 w-100 p-0 m-0'}>
					{submit
						? <CircularProgress size={28} color={'inherit'} className={'d-flex align-items-center justify-content-center text-center'} />
						: 'وارد شوید'}
				</Fab>
			</div>
			<div className={`${classes.signUp} text-center w-100 p-0 m-0`}>
				<span className={'font-weight-300 font-size-16'}>حسابی برای ورود به سیستم ندارید؟ <a className={'font-weight-500'} href={'/'}>یکی بسازید</a></span>
			</div>
		</form>
	);
}

export default Form;