import Form from './Form/Form';
import Technology from '../../../../assets/images/jpg/faizur-rehman.jpg';
import EdwardJoseph from '../../../../assets/images/jpg/edwardjoseph.jpg';
import classes from './Login.scss';

const Login = () => {
	return (
		<div className={`${classes.login} d-flex justify-content-center align-items-center p-0 m-0`}>
			<div className={`${classes.loginInner} overflow-hidden bg-white p-0 m-0`}>
				<div className={'row h-100 p-0 m-0'}>
					<div className={`${classes.formSide} position-relative d-flex justify-content-center align-items-center col-xl-7 col-lg-6 col-12 p-0 m-0`}>
						<div className={`${classes.logo} position-absolute`}>
							<a className={'d-flex align-items-center font-size-16 font-weight-bold'} href={'https://dribbble.com/edward_design'}>
								<span>Edward Joseph</span>
								<img className={'rounded-circle me-2 ms-0 my-0'} src={EdwardJoseph} alt={"Edward Joseph"} />
							</a>
						</div>
						<Form />
					</div>
					<div style={{ backgroundImage: `url(${Technology})` }} className={`${classes.helpSide} position-relative overflow-hidden justify-content-center align-items-center col-xl-5 col-lg-6 d-lg-flex d-none m-0 p-0`}>
						<div className={`${classes.clipContent} position-absolute top-0 end-0 w-100 h-100 p-0 m-0 p-0`}></div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Login;
