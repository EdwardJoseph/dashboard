import { useContext, useState } from 'react';
import AuthContext from '../../../contexts/User/AuthContext'
import InformationContext from '../../../contexts/User/InformationContext'
import Login from './Login/Login';
import Dashboard from './Dashboard/Dashboard';

const Layout = () => {
	// Context
	const Authenticate = useContext(AuthContext);
	const { user } = useContext(InformationContext);

	// State
	const [auth, setAuth] = useState(Authenticate.auth);
	const [userState, setUserState] = useState({
		username: user.username,
		password: user.password,
	});

	return (
		<AuthContext.Provider value={{ auth: auth, login: setAuth }}>
			<InformationContext.Provider value={{ user: userState, setUser: setUserState }}>
				{auth
					? <Dashboard />
					: <Login />}
			</InformationContext.Provider>
		</AuthContext.Provider>
	);
}

export default Layout;
