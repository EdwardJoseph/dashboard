import HashLoader from "react-spinners/HashLoader";
// import classes from './ScreenLoader.scss';

const ScreenLoader = () => {
	return (
		<div className={`d-flex justify-content-center align-items-center position-fixed bg-dark w-100 h-100 top-0 end-0 m-0 p-0`}>
			<HashLoader speedMultiplier={.75} size={72} color={'#039BE5'} />
		</div>
	);
}

export default ScreenLoader;