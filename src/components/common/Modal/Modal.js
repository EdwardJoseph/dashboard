import ReactDOM from 'react-dom'
import classes from './Modal.scss';

const ModalDOM = ({ children }) => {
	return (
		ReactDOM.createPortal(
			<div className={`${classes.modal} d-flex align-items-center justify-content-center fixed-top w-100 h-100 top-0 start-0 p-0 m-0`}>
				<div className={`${classes.backdrop} fixed-top w-100 h-100 top-0 start-0 p-0 m-0`}></div>
				{children}
			</div>
			, document.body)
	);
}

export default ModalDOM;
