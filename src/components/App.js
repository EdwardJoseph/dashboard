import { useEffect, useState } from 'react';
import ScreenLoader from './app/ScreenLoader/ScreenLoader';
import Layout from './app/Layout/Layout';

const App = () => {
	const [screenLoader, setScreenLoader] = useState(true);

	useEffect(() => {
		window.addEventListener('load', () => {
			setScreenLoader(false);
		});
	}, []);

	return (
		<>
			{screenLoader
				? <ScreenLoader />
				: <Layout />}
		</>
	);
}

export default App;
